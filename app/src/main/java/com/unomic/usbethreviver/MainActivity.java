package com.unomic.usbethreviver;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(getApplicationContext(), ReviveService.class);
        startService(intent);
    }
}
