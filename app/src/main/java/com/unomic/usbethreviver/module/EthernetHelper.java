package com.unomic.usbethreviver.module;

import android.net.RouteInfo;
import android.util.Log;

import com.unomic.usbethreviver.UsbEthReviverApp;

import org.apache.commons.net.util.SubnetUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by chostv on 2018. 3. 13..
 */

public class EthernetHelper {

    static String TAG = EthernetHelper.class.getSimpleName();

    //Telechips SDK 소스 참고
    public static final int ETH_STATE_DISABLED = 1;
    public static final int ETH_STATE_ENABLED = 2;
    public static final int ETH_STATE_ENABLING = 3;
    public static final int ETH_STATE_UNKNOWN = 0;
    public static final String ETH_STATE_CHANGED_ACTION =
            "android.net.ethernet.ETH_STATE_CHANGED";
    public static final String NETWORK_STATE_CHANGED_ACTION =
            "android.net.ethernet.STATE_CHANGE";

    final String CFG_PATH = "/sdcard/cfg/eth_cfg.json";
    //이더넷 초기값 (설정값을 못읽을 경우의 초기값)
    String ip_1st, ip_2nd;
    String subnet_1st, subnet_2nd;
    String gateway_1st, gateway_2nd;
    String dns_1st, dns_2nd, dns2_2nd;
    String mode_1st, mode_2nd;

    private Class<?> ethMgrClz;
    private Class<?> devInfoClz;
    private Object ethMgrInst;  //Ethernet Manager 객체
    private Object eth0_devInfoInst; // EthDevInfo 객체
    private Object eth1_devInfoInst;

    private EthernetHelper(){

        try {
            this.ethMgrInst = UsbEthReviverApp.sCtx.getSystemService("ethernet");
            ethMgrClz = Class.forName("android.net.ethernet.EthernetManager");
            devInfoClz = Class.forName("android.net.ethernet.EthernetDevInfo");
            eth0_devInfoInst = devInfoClz.newInstance();
            eth1_devInfoInst = devInfoClz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Ethernet 1st (Defualt Ethernet) 초기값
        ip_1st = "192.168.1.101";
        subnet_1st = "255.255.255.0";
        gateway_1st = "192.168.1.101";
        dns_1st = "0.0.0.0";
        mode_1st = "manual";
        //Ethernet 2nd (Usb to Ethernet) 초기값
        ip_2nd = "192.168.2.102";
        subnet_2nd = "255.255.255.0";
        gateway_2nd = "192.168.2.1";
        dns_2nd = "1.1.1.1";
        dns2_2nd = "8.8.8.8";
        mode_2nd = "manual";
    }

    private static class Singleton{
        private static final EthernetHelper instance = new EthernetHelper();
    }
    public static EthernetHelper getInstance(){
        Log.i(TAG, "Create Instance of EthernetHelper");
        return Singleton.instance;
    }

    void makeDefaultCfgFile() {
        //Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //기본 내장 이더넷
        JSONObject rootJson = new JSONObject();
        //JsonObject rootJson = new JsonObject();
        JSONObject eth1stJson = new JSONObject();
        //JsonObject eth1stJson = new JsonObject();
        //Usb to Ethernet
        JSONObject eth2ndJson = new JSONObject();
       // JsonObject eth2ndJson = new JsonObject();
        //생성자에서 초기화한 기본 값으로 Json 생성
        try {
            eth1stJson.put("ip", ip_1st);
            eth1stJson.put("subnet", subnet_1st);
            eth1stJson.put("gateway", gateway_1st);
            eth1stJson.put("dns", dns_1st);
            eth1stJson.put("mode", mode_1st);
            //Usb to Ethernet 셋팅
            eth2ndJson.put("ip", ip_2nd);
            eth2ndJson.put("subnet", subnet_2nd);
            eth2ndJson.put("gateway", gateway_2nd);
            eth2ndJson.put("dns", dns_2nd);
            eth2ndJson.put("dns2", dns2_2nd);
            eth2ndJson.put("mode", mode_2nd);

            rootJson.put("Eth_1st", eth1stJson);
            rootJson.put("Eth_2nd", eth2ndJson);

            CommonCode.makeFileWithContent(CFG_PATH, rootJson.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }

//        eth1stJson.addProperty("ip", ip_1st);
//        eth1stJson.addProperty("subnet", subnet_1st);
//        eth1stJson.addProperty("gateway", gateway_1st);
//        eth1stJson.addProperty("dns", dns_1st);
//        eth1stJson.addProperty("mode", mode_1st);
        //Usb to Ethernet 셋팅
//        eth2ndJson.addProperty("ip", ip_2nd);
//        eth2ndJson.addProperty("subnet", subnet_2nd);
//        eth2ndJson.addProperty("gateway", gateway_2nd);
//        eth2ndJson.addProperty("dns", dns_2nd);
//        eth2ndJson.addProperty("dns2", dns2_2nd);
//        eth2ndJson.addProperty("mode", mode_2nd);

//        rootJson.add("Eth_1st", eth1stJson);
//        rootJson.add("Eth_2nd", eth2ndJson);
//        CommonCode.makeFileWithContent(CFG_PATH, gson.toJson(rootJson));
    }

    void setIpValueFromFile(){
        //해당경로에 파일이 없을 경우, 디폴트값으로 JSON파일 생성한다
        if(CommonCode.isFileCreated(CFG_PATH) == false){
            makeDefaultCfgFile();
        }else{
            String cfgJsonStr = CommonCode.getFileContent(CFG_PATH);
            //파일에서 읽은 Json데이터를 파싱함
            JSONObject cfgJson = null;
            try {
                cfgJson = new JSONObject(cfgJsonStr);
            } catch (JSONException e) { }

            if(cfgJsonStr != null){
                try {
                    JSONObject eth1stJson = cfgJson.getJSONObject("Eth_1st");
                    JSONObject eth2ndJson = cfgJson.getJSONObject("Eth_2nd");
//                    JsonObject eth1stJson = cfgJson.getAsJsonObject("Eth_1st");
//                    JsonObject eth2ndJson = cfgJson.getAsJsonObject("Eth_2nd");

                    this.ip_1st = eth1stJson.getString("ip");
                    this.subnet_1st = eth1stJson.getString("subnet");
                    this.gateway_1st = eth1stJson.getString("gateway");
                    this.dns_1st = eth1stJson.getString("dns");
                    this.mode_1st = eth1stJson.getString("mode");

                    this.ip_2nd = eth2ndJson.getString("ip");
                    this.subnet_2nd = eth2ndJson.getString("subnet");
                    this.gateway_2nd = eth2ndJson.getString("gateway");
                    this.dns_2nd = eth2ndJson.getString("dns");
                    this.dns2_2nd = eth2ndJson.getString("dns2");
                    this.mode_2nd = eth2ndJson.getString("mode");

//                    this.ip_1st = eth1stJson.get("ip").getAsString();
//                    this.subnet_1st = eth1stJson.get("subnet").getAsString();
//                    this.gateway_1st = eth1stJson.get("gateway").getAsString();
//                    this.dns_1st = eth1stJson.get("dns").getAsString();
//                    this.mode_1st = eth1stJson.get("mode").getAsString();
//
//                    this.ip_2nd = eth2ndJson.get("ip").getAsString();
//                    this.subnet_2nd = eth2ndJson.get("subnet").getAsString();
//                    this.gateway_2nd = eth2ndJson.get("gateway").getAsString();
//                    this.dns_2nd = eth2ndJson.get("dns").getAsString();
//                    this.dns2_2nd = eth2ndJson.get("dns2").getAsString();
//                    this.mode_2nd = eth2ndJson.get("mode").getAsString();
                } catch (JSONException e) {
                    Log.e(TAG, "Json Parsing Failed... Exception message : " + e.getMessage());
                }
                Log.d(TAG, "After Parsing : " + cfgJson);
            }
        }
    }// end setIpValueFromFile

    /**
     * 설정파일에서 값을 읽어와서, 해당 값들로 Ethernet Device (기본 내장 된 이더넷) 의 네트워크 정보를 셋팅함
     */
    void initDefaultEthernetDevice(){
        //설정파일로 부터 값 읽어옴
        setIpValueFromFile();
        try {
            if(mode_1st.equals("manual")){
                setManualConnection(eth0_devInfoInst,"eth0", ip_1st, subnet_1st, gateway_1st, dns_1st);
            }else if(mode_1st.equals("dhcp")){
                setDhcpConn(eth0_devInfoInst,"eth0");
            }else{
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initSecondEthDevice() throws Exception {
        //셋팅되었는지 체크
        boolean isSet = false;
        setIpValueFromFile();
        //현재 사용가능한 Ethernet Device들의 이름 가져옴
        String dvcNameList[] = getDeviceNameList();
        //Log.d(TAG, "Available Device Number : " + dvcNameList.length);
        for(String ethName : dvcNameList){
            Log.d(TAG,"dvcName : " + ethName);
            if(ethName.equals("eth1") || ethName.equals("usb0")){ // Usb to Ethernet
                if(mode_2nd.equals("manual")){
                    setManualConnection(eth1_devInfoInst, ethName, ip_2nd, subnet_2nd, gateway_2nd, dns_2nd);
                    setDnsServerByCommand(ethName, dns_1st, dns_2nd);
                    isSet = true;
                }else if(mode_2nd.equals("dhcp")){
                    setDhcpConn2(eth1_devInfoInst, ethName);
                    isSet = true;
                }else{
                    return;
                }
            }
        }//end for loop
        //위의 셋팅 로직이 안될 경우를 대비하여..
        if(isSet == false) {
            Log.d(TAG, "Eth 2nd Setting incomplete..");
            String path = "/sys/class/net";
            File dirFile = new File(path);
            File[] fileList = dirFile.listFiles();
            for (File tmpFile : fileList) {
                if (tmpFile.getName().equals("usb0") || tmpFile.getName().equals("eth1")) {
                    if (mode_2nd.equals("manual")) {
                        setManualConnection(eth1_devInfoInst, tmpFile.getName(), ip_2nd, subnet_2nd, gateway_2nd, dns_2nd);
                        setDnsServerByCommand(tmpFile.getName(), dns_1st, dns_2nd);
                    } else {
                        return;
                    }
                }
            }
        }
    }
    
    /**
     * System App 권한 필요함..
     * 파라미터로 받은 네트워크 정보들을 이용해, Static IP설정
     * @param dvcName : 인터페이스 이름 example) eth0, eth1
     * @param ip : IP 주소
     * @param netMask : 서브넷마스크 IP
     * @param route : 게이트웨이 IP
     * @param dns : DNS IP
     * @throws Exception
     */
    public void setManualConnection(Object devInfo, String dvcName, String ip, String netMask, String route, String dns) throws Exception {

        // EthDevInfo 객체 생성
        //devInfoInst = devInfoClz.newInstance();

        //IP정보들 셋팅
        Method setIfName = devInfoClz.getMethod("setIfName", String.class);
        Method setIpAddress = devInfoClz.getMethod("setIpAddress", String.class);
        Method setNetMask = devInfoClz.getMethod("setNetMask", String.class);
        Method setRouteAddr = devInfoClz.getMethod("setRouteAddr", String.class);
        Method setDnsAddr = devInfoClz.getMethod("setDnsAddr", String.class);
        Method setConnectMode = devInfoClz.getMethod("setConnectMode", String.class);


        setConnectMode.invoke(devInfo, "manual");
        setIfName.invoke(devInfo, dvcName);
        setIpAddress.invoke(devInfo,ip);
        setNetMask.invoke(devInfo,netMask);
        setRouteAddr.invoke(devInfo,route);
        setDnsAddr.invoke(devInfo, dns);
        //2. UpdateDevInfo
        updateDevInfo(devInfo);
    }

    /**
     * Ethernet I/F HW가 연결되었는지 체크
     * @return 연결된 상태라면 true를, 아니라면 false를 리턴
     * @throws Exception
     */
    public boolean getHWConneted() throws Exception {
        Method methodGetHWConnected = ethMgrClz.getMethod("getHWConnected");
        boolean isHWConnected = (boolean) methodGetHWConnected.invoke(ethMgrInst);
        //Log.d(TAG, "getHWConnected : " + isHWConnected);
        return isHWConnected;
    }

    /**
     * System App 권한 필요함
     * 파라미터로 받은 네트워크 정보들을 이용해, DHCP IP설정
     * @param dvcName : 인터페이스 이름 example) eth0, eth1
     * @throws Exception
     */
    public void setDhcpConn(Object devInfo, String dvcName) throws Exception {

        //devInfoInst = devInfoClz.newInstance();

        Method setIfName = devInfoClz.getMethod("setIfName", String.class);
        setIfName.invoke(devInfo, dvcName);

        //2. UpdateDevInfo
        updateDevInfo(devInfo);
    }

    /**
     * Android System 에서 DHCP를 이용해 할당받은 IP정보를 이용하여, 고정 IP설정을 해주는 메소드
     * 단순히 DHCP기능을 이용하면 라우팅 문제가 발견되어 외부망으로 빠져나가지 못해 인터넷이 이용 불가한
     * 버그를 방지하기 위함임.
     *
     * @param devInfo : Reflection으로 생성된 EthernetDevInfo 인스턴스
     * @param dvcName : Interface 이름 (e.g eth0, eth1)
     * @throws Exception
     */
    public void setDhcpConn2(Object devInfo, String dvcName) throws Exception {

        setDhcpConn(devInfo, dvcName);

        Thread.sleep(1000);

        Class<?> nwUtilsClz = Class.forName("android.net.NetworkUtils");
        Class<?> dhcpRsltClz = Class.forName("android.net.DhcpResults");
        //NetworkUtils 객체 생성
        Object nwUtilInst = nwUtilsClz.newInstance();
        //DhcpResults 객체 생성
        Object dhcpRsltInst = dhcpRsltClz.newInstance();
        //NetworkUtils의 DHCP관련 메소드
        Method runDhcp = nwUtilsClz.getMethod("runDhcp", String.class, dhcpRsltClz);
        Method stopDhcp = nwUtilsClz.getMethod("stopDhcp", String.class);
        Method releaseDhcp = nwUtilsClz.getMethod("releaseDhcpLease", String.class);

        //DHCP STOP & START
        runDhcp.setAccessible(true);
        stopDhcp.setAccessible(true);
        //releaseDhcp.invoke(nwUtilInst, dvcName);
        stopDhcp.invoke(nwUtilInst, dvcName);
        runDhcp.invoke(nwUtilInst, dvcName, dhcpRsltInst);

        Log.i(TAG, "DHCP RESTART FINISH");

        //위의 DHCP Restart 결과로 인해 DhcpResults 객체에 담긴 주소정보 가져옴
        Field linkProp = dhcpRsltClz.getField("linkProperties");
        //LinkProperties 이용 IP정보 가져오기
        Class<?> linkPropClz = Class.forName("android.net.LinkProperties");
        Object linkPropInst = linkProp.get(dhcpRsltInst);
        //LinkProperties에 들어있는 IP주소, Route 관련 정보, DNS정보를 가져온다
        Method getAddresses = linkPropClz.getMethod("getAddresses");
        Method getRoutes = linkPropClz.getMethod("getRoutes");
        Method getDnses = linkPropClz.getMethod("getDnses");

        Collection<InetAddress> address = (Collection<InetAddress>) getAddresses.invoke(linkPropInst);
        Collection<RouteInfo> routes = (Collection<RouteInfo>) getRoutes.invoke(linkPropInst);
        Collection<InetAddress> dnses = (Collection<InetAddress>) getDnses.invoke(linkPropInst);
        List<InetAddress> addrList = new ArrayList<>(address);
        List<RouteInfo> routeList = new ArrayList<>(routes);
        List<InetAddress> dnsList = new ArrayList<>(dnses);

        //Log.i(TAG, "DHCP RESTART FINISH");

        String dhcpIpAddr = addrList.get(0).getHostAddress();
        String dhcpGateWay = "";
        try {
            for(RouteInfo routeInfo : routeList) {
                if(routeInfo.isDefaultRoute()) {
                   dhcpGateWay = routeInfo.getGateway().getHostAddress();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Class<?> routeInfoClz = Class.forName("android.net.RouteInfo");
        Object routeInfoInst = routeList.get(0);
        Method getDestination = routeInfoClz.getMethod("getDestination");
        Class<?> linkAddrClz = Class.forName("android.net.LinkAddress");
        Object linkAddrInst = getDestination.invoke(routeInfoInst);
        Method getNetworkPrefixLength = linkAddrClz.getMethod("getNetworkPrefixLength");
        int prefixLength = (int) getNetworkPrefixLength.invoke(linkAddrInst);

        //Subnet Util 이용해서 Netmask String 생성
        SubnetUtils subnetUtils = new SubnetUtils(dhcpIpAddr + "/" + prefixLength);

        String dhcpSubNet = subnetUtils.getInfo().getNetmask();
        String dhcpDNS = dnsList.get(0).getHostAddress();
        String dhcpDNS2 = dnsList.get(1).getHostAddress();

        Log.d("DHCP to Manual", "Dvc Name : " + dvcName);
        Log.d("DHCP to Manual", "IP : " + dhcpIpAddr);
        Log.d("DHCP to Manual", "GW : " + dhcpGateWay);
        Log.d("DHCP to Manual", "Subnet : " + dhcpSubNet);
        Log.d("DHCP to Manual", "DNS : " + dhcpDNS);

        //DHCP모드는 중단하고, 메뉴얼로 재설정 한다.
        Log.d("DHCP to Manual", "Stop DHCP : " + stopDhcp.invoke(nwUtilInst, dvcName));
        Log.d("DHCP to Manual", "Release DHCP : " + releaseDhcp.invoke(nwUtilInst, dvcName));

        //setEthernetStateEnable();
        setManualConnection(devInfo, dvcName, dhcpIpAddr, dhcpSubNet, dhcpGateWay, dhcpDNS);
        setDnsServerByCommand(dvcName, dhcpDNS, dhcpDNS2);
    }

    /**
     * System App 권한 필요함
     * @param devInfo : IP정보들이 담긴 EthDevInfo 객체
     * @throws Exception
     */
    private void updateDevInfo(Object devInfo) throws Exception {
        //Eth Enable 먼저
        //setEthEnabled();
        Method updateEthDevInfo = ethMgrClz.getMethod("UpdateEthDevInfo", devInfoClz);
        updateEthDevInfo.setAccessible(true);
        updateEthDevInfo.invoke(ethMgrInst, devInfo);

        //Enable
        setEthernetStateEnable();
    }

    /**
     * System App 권한 필요함
     * 이더넷 I/F들 상태들을 Enable로 만듬
     *
     * @throws Exception
     */
    public void setEthernetStateEnable() throws Exception {
        Method setEthernetState = ethMgrClz.getMethod("setEthernetState", Integer.TYPE);
        setEthernetState.setAccessible(true);
        setEthernetState.invoke(ethMgrInst, ETH_STATE_ENABLED);
    }
    private void setEthEnabled() throws Exception {
        Method setEthEnabled = ethMgrClz.getMethod("setEthEnabled", Boolean.TYPE);
        setEthEnabled.setAccessible(true);
        setEthEnabled.invoke(ethMgrInst, true);
    }

    /**
     * System App 권한 필요함
     * 이더넷 I/F들 상태를 Disable로 만듬
     * @throws Exception
     */
    public void setEthernetStateDisable() throws Exception {
        Method setEthernetState = ethMgrClz.getMethod("setEthernetState", Integer.TYPE);
        setEthernetState.setAccessible(true);
        setEthernetState.invoke(ethMgrInst, ETH_STATE_DISABLED);
    }

    /**
     * System App 권한 필요함
     * 이더넷 I/F들 상태를 읽는다
     *
     * @throws Exception
     */
    public int getEthernetState() throws Exception {
        Method getEthState = ethMgrClz.getMethod("getEthState");
        int ethState = (int) getEthState.invoke(ethMgrInst);
        Log.d(TAG, "getEthernetState : " + ethState);
        return ethState;
    }

    public int getCheckConnecting() throws Exception {
        Method getCheckConnecting = ethMgrClz.getMethod("getCheckConnecting");
        int connStat = (int) getCheckConnecting.invoke(ethMgrInst);
        Log.d(TAG, "getCheckConnecting : " + connStat);
        return connStat;
    }

    public boolean getStackConnected() throws Exception {
        Method getStackConnected = ethMgrClz.getMethod("getStackConnected");
        boolean stackConn = (boolean) getStackConnected.invoke(ethMgrInst);
        //Log.d(TAG, "getStackConnected : " + stackConn);
        return stackConn;
    }

    public boolean isEthConfigured() throws Exception {
        Method isEthConfigured = ethMgrClz.getMethod("isEthConfigured");
        boolean configConn = (boolean) isEthConfigured.invoke(ethMgrInst);
        Log.d(TAG, "isEthConfigured : " + configConn);
        return configConn;
    }

    /**
     * 현재 사용가능한 Ethernet Device 들의 이름들을 가져온다
     * @return Device Name 이 담긴 String 배열
     */
    String[] getDeviceNameList(){
        //현재 접속된 eth device목록들 가져오는 메소드
        String[] dvcList = null;
        try {
            Method methodGetDeviceNameList = ethMgrClz.getMethod("getDeviceNameList");
            dvcList = (String[]) methodGetDeviceNameList.invoke(ethMgrInst);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dvcList;
    }

    /**
     * Commmand를 이용해서 DNS서버를 셋팅하는 메소드
     * @param dns1 : DNS 서버 주소 1
     * @param dns2 : DNS 서버 주소 2
     */
    void setDnsServerByCommand(String dvcName, String dns1, String dns2) throws Exception {

        Process process = Runtime.getRuntime().exec(new String[] {"ndc", "resolver", "flushdefaultif"});
        process.waitFor();
        BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
        System.out.println(br.readLine());

        Process process2 = Runtime.getRuntime().exec(new String[] {"ndc", "resolver", "flushif", dvcName});
        process2.waitFor();
        BufferedReader br2 = new BufferedReader(new InputStreamReader(process2.getInputStream()));
        System.out.println(br2.readLine());

        Process process3 = Runtime.getRuntime().exec(new String[] {"ndc", "resolver", "setdefaultif", dvcName});
        process3.waitFor();
        BufferedReader br3 = new BufferedReader(new InputStreamReader(process3.getInputStream()));
        System.out.println(br3.readLine());

        Process process4 = Runtime.getRuntime().exec(new String[] {"ndc", "resolver", "setifdns", dvcName, dns1, dns2});
        process4.waitFor();
        BufferedReader br4 = new BufferedReader(new InputStreamReader(process4.getInputStream()));
        System.out.println(br4.readLine());
    }

    /**
     * EthernetStateTracker에 있는 메소드를 복사함
     *
     * @param addrString : Ip Address String
     * @return : int로 변경된 IP주소
     * @throws UnknownHostException
     */
    int stringToIpAddr(String addrString) throws UnknownHostException {
        try {
            if (addrString == null)
                return 0;
            String[] parts = addrString.split("\\.");
            if (parts.length != 4) {
                throw new UnknownHostException(addrString);
            }

            int a = Integer.parseInt(parts[0]);
            int b = Integer.parseInt(parts[1]) <<  8;
            int c = Integer.parseInt(parts[2]) << 16;
            int d = Integer.parseInt(parts[3]) << 24;

            return a | b | c | d;
        } catch (NumberFormatException ex) {
            throw new UnknownHostException(addrString);
        }
    }
}
