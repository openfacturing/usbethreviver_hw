package com.unomic.usbethreviver.module;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.unomic.usbethreviver.ReviveService;


/**
 * Created by chostv on 2018. 2. 1..
 */
//Android System 단에서 강제종료 시, 다시 재시작 시키는 로직
//https://stackoverflow.com/questions/19945407/can-i-call-a-method-before-my-application-go-to-crash/19945692
public class ServiceReviveHandler implements Thread.UncaughtExceptionHandler {

    static final String TAG = ServiceReviveHandler.class.getSimpleName();

    private Context mContext;

    private ServiceReviveHandler(Context context) {
        mContext = context;
    }

    public static void attach(Context context) {

        Log.i(TAG, "Attached ServiceReviveHandler");
        Thread.setDefaultUncaughtExceptionHandler(
                new ServiceReviveHandler(context)
        );

    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {

        Log.e(TAG, "ERROR IS " + (e));
        e.printStackTrace();
        Intent intent = new Intent(mContext, ReviveService.class);
        //실행되고 있는 서비스 중단
        mContext.stopService(intent);

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e1) {}

        Log.i(TAG, "Restart ReviverService");
        //다시 서비스 시작
        mContext.startService(intent);

        //프로그램 종료시키는 코드로 추정됨
        //from RuntimeInit.crash()
        //Process.killProcess(Process.myPid());
        //System.exit(10);
    }
}
