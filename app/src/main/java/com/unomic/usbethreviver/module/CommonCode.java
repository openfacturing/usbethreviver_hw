package com.unomic.usbethreviver.module;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by bong on 2015-06-13.
 *
 * Modified by Steve on 2018-08-22
 */
public class CommonCode {

    public static final String TAG = CommonCode.class.getSimpleName();

    public static void makeFileCat(String filename, String content) {
        try {
            File file = new File(filename);
            FileWriter fw = new FileWriter(file, true);

            fw.write(content);
            fw.flush();
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void makeFileNoCat(String filename, String content) {
        try {
            File file = new File(filename);
            FileWriter fw = new FileWriter(file, false);

            fw.write(content);
            fw.flush();
            fw.close();
        } catch (Exception e) {
            Log.e(TAG, filename + "Making Fail..", e);
        }
    }

    /**
     * path에 폴더가 있는지 체크하고, 없으면 생성
     *
     * @param path 경로
     */
    public static void makeFolder(String path) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }
        } catch (Exception e) {
            Log.e(TAG, path + "Making Fail..", e);
        }
    }

    public static void makeFileWithContent(String filename, String content) {
        content = content + "\n";
        try {
            File fileDir = new File(filename.substring(0,filename.lastIndexOf("/")));
            //폴더가 없을 경우 생성
            if(!fileDir.exists()){
                fileDir.mkdir();
            }
            File file = new File(filename);
            FileWriter fw = new FileWriter(file, false);
            fw.write(content);
            fw.flush();
            fw.close();
        } catch (Exception e) {
            Log.e(TAG, filename + "Making Fail..", e);
        }
    }

    public static boolean isFileCreated(String filename) {
        File file = new File(filename);
        return file.exists();
    }

    public static String getFileContent(String filename) {
        File file = new File(filename);
        if (!isFileCreated(filename)) {
            Log.d("GLOBALS", "no File exists");
            return null;
        }

        String ret = "";

        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = br.readLine()) != null) {
                ret += line;
            }
            br.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return ret;
        }
    }

    public static String getToday() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    public static String getTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    public static String getTimeMilisec() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }

    public static String getTime(long milisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(milisecond);
        return sdf.format(date);
    }

    public static String getLogTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis() + (3600 * 1000));
        return sdf.format(date);
    }

    public static String getLogTime(long milisecond) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(milisecond + (3600 * 1000));
        return sdf.format(date);
    }

    public static boolean isIntegerParseInt(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException nfe) {
        }
        return false;
    }

    /**
     * 현재 실행되고 있는 서비스 목록을 가져와서, 파라미터 이름으로 서비스이름
     * @param context
     * @param serviceName 비교하고 싶은 서비스 이름  ex) mtadapter.mtadapter.MyService2
     * @return serviceName으로 받는 서비스가 실행되고 있으면, true 아니면 false 리턴
     */
    public static boolean isServiceRunningCheck(Context context, String serviceName) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    //http://codeticker.tistory.com/entry/Android-%ED%98%84%EC%9E%AC-%EC%8B%A4%ED%96%89%EC%A4%91%EC%9D%B8-%ED%94%84%EB%A1%9C%EC%84%B8%EC%8A%A4-%EB%AA%A9%EB%A1%9D-%EC%96%BB%EA%B8%B0
    /**
     * 현재 실행되고 있는 프로세스 목록을 가져와서, 인자로 받는 프로세스가 실행되고 있는지 비교한다
     * @param context
     * @param processName 비교할 process이름   ex) mtadapter.mtadapter:remote
     * @return processName으로 받는 프로세스가 실행되고 있으면 true 아니면, false 리턴
     */
    public static boolean isProcessRunningCheck(Context context, String processName){
        ActivityManager am = (ActivityManager)context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> processList = am.getRunningAppProcesses();

        for(ActivityManager.RunningAppProcessInfo process : am.getRunningAppProcesses()){
            if(processName.equals(process.processName)){
                return true;
            }
        }
        return false;

    }

    /**
     * Log를  "/sdcard/파일명.log" 에 찍는다
     *
     * @param logFileName : log파일명
     * @param content    : 찍을 내용
     */
    public static void appendLog(String logType, String logFileName, String content) {
        String logStr = "(" + logType + ") " + getTime() + "  :  " + content;
        File logFolder = new File("/sdcard/log");

        File logFile = new File("/sdcard/log/" + logFileName + ".log");

        // "/sdcard/log 폴더가 없을 경우 생성한다"
        if (!logFolder.exists()) {
            try {
                logFolder.mkdirs();
            } catch (Exception e) {
                Log.e(TAG, "Exception occured... realted to creating log folder..", e);
            }
        }

        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, "IOException occured... realted to creating log file..", e);
            }
        }

        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(logFile, true));
            bw.append(logStr);
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            Log.e(TAG, "IOException occured... related to BufferedWriter", e);
        }
    }

    /**
     * 기존에 생성된 Log파일 삭제
     *
     * @param logFileName : log파일명
     */
    public static void deleteLogFile(String logFileName) {
        File logFolder = new File("/sdcard/log");
        File logFile = new File("/sdcard/log/" + logFileName + ".log");
        if(logFolder.exists() == false){
            return;
        }
        if(logFile.exists() == true){
            logFile.delete();
        }
    }


    /**
     * @param t : Exception
     * @return : Exception 을 String으로 변환한 것
     */
    public static String stackTraceToString(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        String stackTraceStr = sw.toString();

        return stackTraceStr;
    }

    /**
     * 인터넷 연결 Check 메소드
     * @param ctx getApplicationContext를 넣으면 됨
     * @return
     */
    public static boolean chkInternetConnect(Context ctx){
        //http://hashcode.co.kr/questions/2135/%EC%95%88%EB%93%9C%EB%A1%9C%EC%9D%B4%EB%93%9C%EC%97%90%EC%84%9C-%EC%99%80%EC%9D%B4%ED%8C%8C%EC%9D%B4-%EC%97%B0%EA%B2%B0%EB%90%90%EB%8A%94%EC%A7%80-%ED%99%95%EC%9D%B8%ED%95%98%EB%8A%94-%EB%B2%95

        ConnectivityManager cm = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiStat = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isConnected = (wifiStat != null && wifiStat.isConnectedOrConnecting());
        return isConnected;
    }

    /**
     * 기존에 MTConnect 관련 App (MTAdpater, MTAgent, MTCenter)들이 사용하는 설정파일에서
     * 서버로 요청하는 주소를 파싱하여, 서버주소를 리턴함
     * @return 파싱한 서버주소
     */
    public static String getlegacyServerUrl(){
        //MTAdapter, MTAgent, MTCenter App들이 사용하는 설정파일 경로
        String cfgFilePath = "/sdcard/config.txt";
        String cfgJson = null;
        if(isFileCreated(cfgFilePath)){
            cfgJson = getFileContent(cfgFilePath);
        }else{
            Log.e(TAG + "_getlegacyServerUrl", "-config.txt- is not found");
            return null;
        }

        if(cfgJson != null) {
            JSONObject rootJson;
            String serverUrl;
            try {
                rootJson = new JSONObject(cfgJson);
                JSONArray centerArr = rootJson.getJSONArray("MtCenter");
                JSONObject centerJson = centerArr.getJSONObject(0);
                //"MtCenter" Json Object에서 Key가 request_server_address인 값을 가져온다.
                //해당 값이 공작기계 데이터를 서버로 전송하는 Rest API url 이다.
                //이런식으로 되어 있다.. "http://192.168.6.10/UNS/mtc/mtcCrnt.do"
                String tmp = centerJson.getString("request_server_address");
                //UNS를 문자열의 끝에서 부터 찾아서 서브 스트링을 만듬
                serverUrl = tmp.substring(0, tmp.lastIndexOf("UNS"));
                Log.d(TAG + "_getlegacyServerUrl", "Server Url : " + serverUrl);
                return serverUrl;
            } catch (JSONException e) {
                Log.e(TAG + "_getlegacyServerUrl", "JSONException", e);
            }
        }
        return null;
    }

    /**
     * 안드로이드에 설치된 App을 실행 시켜주는 메소드
     * @param appName : App의 Package포함 전체 이름
     */
    public static void executeAppliation(String appName, Context ctx){

        //설치되어있는 앱을 실행해주는 메소드
        //https://www.fun25.co.kr/blog/android-execute-3rdparty-app/?category=003
        Intent intent = ctx.getPackageManager().getLaunchIntentForPackage(appName);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(intent);
    }

    /**
     * 이더넷 IP 셋팅이 정상적으로 되어 있는지 확인하는 메소드
     * @return IP 셋팅 상태
     */
    public static boolean isEthOn() {
        try {
            String line;
            boolean isEthSet = false;
            Process p = Runtime.getRuntime().exec("netcfg");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                if(line.contains("eth0")){
                    if(line.contains("0.0.0.0")){
                        isEthSet = false;
                    } else {
                        isEthSet = true;
                    }
                }
            }
            input.close();
            Log.e(TAG + "_isEthOn","isEthOn: "+isEthSet);
            return isEthSet;

        } catch (IOException e) {
            Log.e(TAG + "_isEthOn","Runtime Error: "+e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 두번째 이더넷포트 (USB to Ethernet)의
     * IP 셋팅이 정상적으로 되어 있는지 확인하는 메소드
     * @return IP 셋팅 상태
     */
    public static boolean isEth2On() {
        try {
            String line;
            boolean isEthSet = false;
            Process p = Runtime.getRuntime().exec("netcfg");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                //USB to Ethernet을 usb0 또는 eth1 으로 인식하기 때문에
                if(line.contains("usb0") || line.contains("eth1")){
                    //netcfg를 해서 필터링한 결과물에 ip가 DOWN이거나 0.0.0.0 일 경우
                    if(line.contains("0.0.0.0") || line.contains("DOWN")){
                        isEthSet = false;
                    } else {
                        isEthSet = true;
                    }
                }
            }
            input.close();
            //Log.e(TAG + "_isEth2On","isEth2On: "+isEthSet);
            return isEthSet;

        } catch (IOException e) {
            Log.e(TAG + "_isEth2On","Runtime Error: "+e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
