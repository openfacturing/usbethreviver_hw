package com.unomic.usbethreviver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class RestartReceiver extends BroadcastReceiver {

    static final String TAG_LOG = "UsbEthReviver" + RestartReceiver.class.getSimpleName();
    public static final String ACTION_RESTART_PERSISTENTSERVICE = "ACTION.Restart.PersistentService";

    @Override
    public void onReceive(Context context, Intent intent) {
        /**
         * 단말 재시작 시 서비스 등록
         */
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Log.i(TAG_LOG, "ACTION_BOOT_COMPLETED");
            //Initial Service실행
            Intent i = new Intent(context, ReviveService.class);
            context.startService(i);
        }
    }
}
