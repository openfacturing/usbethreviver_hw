package com.unomic.usbethreviver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.unomic.usbethreviver.module.CommonCode;

import com.unomic.usbethreviver.module.EthernetHelper;
import com.unomic.usbethreviver.module.ServiceReviveHandler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReviveService extends Service {

    static String TAG = ReviveService.class.getSimpleName();
    // 서비스 종료시 재실행 딜레이 시간, activity의 활성 시간을 벌어야 한다.
    private static final int RESTART_DELAY_TIMER = 10 * 1000;
    ReviveThread reviveThread;
    EthernetHelper ethernetHelper;

    // 유노스 서버 접속 실패 카운트
    static int disconnect_count = 0;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        //시스템에서 서비스 강제종료시 재실행 되는 로직 실행 가능하게 하는 클래스
        ServiceReviveHandler.attach(getApplicationContext());
        //기존에 등록된 알람 해제 (서비스 재실행을 위한 알람)
        unregisterRestartAlarm();
        reviveThread = new ReviveThread();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        reviveThread.start();
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onDestroy() {
        reviveThread.interrupt();
        registerRestartAlarm();
        super.onDestroy();
    }

    private void registerRestartAlarm(){
        Intent intent = new Intent(getApplicationContext(), RestartReceiver.class);
        intent.setAction(RestartReceiver.ACTION_RESTART_PERSISTENTSERVICE);
        PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
        //종료시 Term 계산을 위해 을 주기위해서 부팅시간 획득
        long firstTime = SystemClock.elapsedRealtime();
        firstTime += RESTART_DELAY_TIMER;

        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,  firstTime, RESTART_DELAY_TIMER, sender);

    }

    /**
     * 기존에 등록된 Alarm 해제
     */
    private void unregisterRestartAlarm(){
        Intent intent = new Intent(getApplicationContext(), RestartReceiver.class);
        intent.setAction(RestartReceiver.ACTION_RESTART_PERSISTENTSERVICE);
        PendingIntent sender = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        am.cancel(sender);
    }


    public class ReviveThread extends Thread{
        @Override
        public void run() {

            System.out.println("USB ETH revier Start");

            while(true){

                ethernetHelper = EthernetHelper.getInstance();

                Log_Eth_info(eth_info());

                if(!UnosServer_IsConnect()) {
                    disconnect_count += 1;
                } else {
                    disconnect_count = 0;
                }

                System.out.println("UnosServer Disconnect count : " + disconnect_count);

                // 서버 접속 실패 회수가 5회 이상(1분 이상) 일 시, eth Down 시키기 (19.04.02 테스트 시, usb to eth 만 Down 됨)
                if(disconnect_count > 2) {
                    try{
                        ethernetHelper.setEthernetStateDisable();
                        disconnect_count = 0;

                        System.out.println("Eth Down !!!!!");
                        Log_IsConnect("Eth Down");
                        Thread.sleep(5000);
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }


                try {
                    //Ethernet IP 셋팅체크..
                    if(!CommonCode.isEth2On()){
                        ethernetHelper.initSecondEthDevice();

                        Log_IsConnect("Eth UP");
                    }

                    //10초간 Term
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    break;
                } catch (Exception e){
                    e.printStackTrace();
                }

            }//end while loop
        }//end run method
    }//end ReviveThread

    public static boolean UnosServer_IsConnect() {

        boolean result;

        String server_url = "55.111.150.201";
        int server_port = 8000;

        System.out.println("Socket Connecting");
        Socket client = new Socket();
        try {
            client.setSoTimeout(2000);
            client.connect(new InetSocketAddress(server_url,server_port),2000);
            result = true;
        } catch (Exception e){
            e.printStackTrace();
            result = false;
        }
        try{
            client.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        Log_IsConnect(Boolean.toString(result) + ",  count : " + disconnect_count);
        System.out.println("Socket Close");
        return result;

    }

    public static void Log_IsConnect(String log){
        try {
            String path = "/sdcard/";
            File downld_path = new File(path);

            if (!downld_path.exists()) {
                downld_path.mkdir();
            }

            File log_file = new File(path + "con.log");

            if (log_file.exists()==false) {
                log_file.createNewFile();
            } else {
                if (log_file.length() > 5000000) {
                    log_file.delete();
                    log_file.createNewFile();
                }
            }

            BufferedWriter buf = new BufferedWriter(new FileWriter(log_file, true));
            buf.append(getTime() + ".  " + log);
            buf.newLine();
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static String getTime() {
        long time = System.currentTimeMillis();
        SimpleDateFormat dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss(SSS)");
        String str = dayTime.format(new Date(time));
        return str;
    }

    public static String eth_info() {


        try {
            String result = "";

            String line;
            boolean isEthSet = false;
            Process p = Runtime.getRuntime().exec("netcfg");
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

            while ((line = input.readLine()) != null) {

                if(line.contains("eth0")){
                    result += line + "\n";
                }

                if(line.contains("usb0") || line.contains("eth1")){
                    result += line + "\n";
                }
            }

            input.close();

            return result;

        } catch (IOException e) {
            e.printStackTrace(); return "no Line";
        }

    }

    public static void Log_Eth_info(String log){
        try {
            String path = "/sdcard/";
            File downld_path = new File(path);

            if (!downld_path.exists()) {
                downld_path.mkdir();
            }

            File log_file = new File(path + "eth_info.log");

            if (log_file.exists()==false) {
                log_file.createNewFile();
            } else {
                if (log_file.length() > 5000000) {
                    log_file.delete();
                    log_file.createNewFile();
                }
            }

            BufferedWriter buf = new BufferedWriter(new FileWriter(log_file, true));
            buf.append(getTime() + ".  \n" + log + "\n");
            buf.newLine();
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
