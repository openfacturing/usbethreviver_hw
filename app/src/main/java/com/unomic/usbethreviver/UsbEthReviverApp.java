package com.unomic.usbethreviver;

import android.app.Application;
import android.content.Context;

public class UsbEthReviverApp extends Application {
    public static Context sCtx;

    @Override
    public void onCreate() {
        super.onCreate();
        sCtx = getApplicationContext();
    }

}
